<?php
require_once(dirname(__FILE__).'/../cookie/JdUser.php');
/**
 * 登录拦截器，将未登录过的用户重定向到登录页
 * User: zoutuo
 * Date: 14-10-16
 * Time: 上午10:53
 */
class LoginFilter extends CFilter
{
    /**
     * ERP账号登录信息
     */
    private $erpDomain;
    private $erpUrl;

    /**
     * cookie秘钥信息
     */
    private $erpKey;

    /**
     * 初始化工作
     */
    public function init()
    {
        $config = Yii::app()->getComponents(false);
        //域名信息
        $this->erpDomain = $config['decryption']['erp_cookie_domain'];
        //url信息
        $this->erpUrl = $config['decryption']['erp_login_url'];
        //秘钥信息
        $this->erpKey = $config['decryption']['erp_cookie_key'];
    }

    /**
     * 拦截器主方法
     * @param CFilterChain  过滤器链
     * @return bool         是否过滤成功
     */
    protected function preFilter($filterChain)
    {
        if (!empty ($_COOKIE[$this->erpDomain])) {
            $this->setCookieLogin($_COOKIE[$this->erpDomain], $this->erpKey);
        }
        return $this->preFilterErpLogin();
    }

    /**
     * 拦截请求，判断是否已经ERP单点登录过
     */
    private function preFilterErpLogin()
    {
        if (Yii::app()->request->cookies[$this->erpDomain]) {
            return true;
        }
        return $this->redirectToLogin($this->erpUrl);
    }

    /**
     * 检查COOKIE，没有的话就跳转到登录页面
     * @param $loginUrl 登录URL
     * @internal param  $cookieDomain 的域名
     * @return bool     是否已登录
     */
    private function redirectToLogin($loginUrl)
    {
        $currentHost = Yii::app()->request->hostInfo;
        $currentUrl = Yii::app()->request->getUrl();
        $returnUrl = $currentHost . $currentUrl;
        $redirectUrl = $loginUrl . urlencode($returnUrl);
        Yii::app()->request->redirect($redirectUrl);
        return false;

    }

    /**
     * COOKIE解密，记录登录用户类型
     * @param $clientCookie 登录用户cookie
     * @param $key cookie秘钥
     */
    private function setCookieLogin($clientCookie, $key)
    {
        $client = new JdUser ();
        $client->init_user($clientCookie, $key);
        $name = iconv("GB2312", "UTF-8", $client->username);
        Yii::app()->session['clientname'] = $name;
    }

}