<?php
namespace Home\Common;
class JdUser {
	
	public $username = ''; //用户名
	public $appPath = ""; //应用路径
	public $userData;
	public $createTime = ""; //创建时间
	public $expiration = "";
	public $isPersistent = "";
	public $version = ""; //版本
	

	/** 获取用户名*/
	public function init_user($cookie, $key) {
		
//		require_once './JdCryptAES.php'; //引入AES加解密文件
		/**1.获取解码后的ASCLL数组*/
		$aes = new \Home\Common\JdCryptAES ();
		$aes->set_key ( $key );
		$aes->require_pkcs5 ();
		
		$ticketBytes = $aes->decrypt ( $cookie ); //获取ASCLL数组
		/**2.解析用户信息*/
		
		try{
			self::decodeUserInfo ( $ticketBytes );
		}catch (Exception $e){
			exit("error code:d001");
		}
	
	}
	
	/**
	 * 从ASCLL数组中解析出用户信息
	 * @param  $ticketBytes
	 */
	function decodeUserInfo($ticketBytes) {
		
		$pos = 8;
		
		$version = $ticketBytes [$pos];
		$pos ++;
		
		//分析出用户名。utf16-le编码是两字节。所以结束也是0000
		$usernames = self::readUtf16le ( $ticketBytes, $pos );
		$pos += count ( $usernames ) + 2;
		
		//分析创建时间
		$createTimeBytes = array ();
		
		$createTimeBytes = self::arraycopy ( $ticketBytes, $pos, $createTimeBytes, 0, 8 );
		$pos += count ( $createTimeBytes );
		
		//分析是否持久
		$isPersist = $ticketBytes [$pos];
		$pos ++;
		
		//分析出过期时间
		$expireTimeBytes = array ();
		$expireTimeBytes = self::arraycopy ( $ticketBytes, $pos, $expireTimeBytes, 0, 8 );
		$pos += count ( $expireTimeBytes );
		
		//分析出userdata
		$userdatas = self::readUtf16le ( $ticketBytes, $pos );
		$pos += count ( $userdatas ) + 2;
		
		//分析出路径
		

		$paths = self::readUtf16le ( $ticketBytes, $pos );
		$pos += count ( $paths ) + 2;
		
		$this->username = self::getDecodeStr ( $usernames );
		
		$this->userData = self::getDecodeStr ( $userdatas );
		
		$this->appPath = self::getDecodeStr ( $paths );
		
		$this->createTime = self::byteArrayToDate ( $createTimeBytes );
		
		$this->expiration = self::byteArrayToDate ( $expireTimeBytes );
	
	}
	
	/**
	 * 从解密后字串中得出utf16le编码内容。utf16le双字节存储，以连续0000结尾
	 *
	 * @param ticketBytes
	 * @param start       ticketBytes的起始位置
	 * @return 单独内容。长度
	 */
	function readUtf16le($ticketBytes, $start) {
		$end = self::checkUtf16leEnd ( $ticketBytes, $start );
		if ($end < 0) {
			return array (0 );
		} else {
			$len = $end - $start;
			$desc = array ();
			$desc = self::arrayCopy ( $ticketBytes, $start, $desc, 0, $len );
			return $desc;
		}
	}
	
	function checkUtf16leEnd($ticketBytes, $start) {
		
		$byteLen = count ( $ticketBytes );
		$end = $byteLen - $start; //默认到末尾
		

		for($i = $start; $i < $byteLen - 1; $i += 2) {
			$i1 = $ticketBytes [$i];
			$i2 = $ticketBytes [$i + 1];
			if ($i1 == 0 && $i2 == 0) {
				$end = $i;
				break;
			}
		}
		return $end;
	}
	
	/** 根据ASCLL数组解码出时间*/
	public function byteArrayToDate($bytes) {
		
		if (count ( $bytes ) != 8)
			throw new Exception ( "must be 8 bytes" );

        $str = "";

		for($i = count ( $bytes ) - 1; $i >= 0; $i --) {
			
			$decstr = dechex ( $bytes [$i] ); //从10进制转换为16进制

			$decsub = substr ( $decstr, strlen ( $decstr ) - 2, 2 ); //截取后两字符
			
			if (1 == strlen ( $decsub ) && $i != count ( $bytes ) - 1) { //除了第一个字符外，其余的如果只为一个字符则在前边补0
				$str .= "0" . $decsub;
			} else {
				$str .= $decsub;
			}
		
		}
		
		$data = base_convert ( iconv ( 'gbk', 'utf-8', $str ), 16, 10 );
		
		$date = date ( "Y-m-d H:i:s", (($data - 116444736000000000) / 10000) / 1000 );
		
		return $date;
	}
	
	/** 根据数组转换成字符串*/
	public function getDecodeStr($bytes) {
		$result = '';
		
		for($i = 0; $i < count ( $bytes );) {
			
			$str = chr ( $bytes [$i] );
			if (! empty ( $bytes [$i + 1] )) {
				$str .= chr ( $bytes [$i + 1] );
			}
			
			if (strlen ( $str ) != 1) {
				$result .= iconv ( 'UTF-16LE', 'utf-8', $str ); //将字符串从UTF-16LE转换回UTF-8
			} else {
				$result .= $str;
			}
			$i = $i + 2;
		}
		
		return trim ( $result ); //去空格
	}
	
	/** 
	 * 数组拷贝
	 * $srcArray  源数组
	 * $srcPos    源数组拷贝起点
	 * $desArray  目标数组
	 * $destPos   目标数组拷贝起点
	 * $byteCount 拷贝长度
	 **/
	public function arrayCopy($srcArray, $srcPos, $desArray, $destPos, $byteCount) {
		for($i = 0; $i < $byteCount; $i ++) {
			$desArray [$destPos] = $srcArray [$srcPos];
			$srcPos ++;
			$destPos ++;
		}
		
		return $desArray;
	}
}

?>
