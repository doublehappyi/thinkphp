<?php
namespace Admin\Controller;

use \Common\Controller\SoprController;

class ModuleController extends SoprController{
    const MODULE_KEY = 'Module';
    public function index(){
        parent::index();
        $this->display();
    }

    public function addItem(){
        parent::addItem();
        //获取参数
        $moduleKey = trim(I('moduleKey', ""));
        $moduleName = trim(I("moduleName", ""));
        $moduleUrl = trim(I("moduleUrl", ""));
        $syncFile = trim(I("syncFile", ""));
        $isDelete = intval(trim(I("isDelete", 0)));

        if(!isNoEmptyString($moduleKey)){
            $this->ajaxReturnError("moduleKey字段非法");
        }
        if(!isNoEmptyString($moduleName)){
            $this->ajaxReturnError("moduleName字段非法");
        }
        if(!isString($moduleUrl)){
            $this->ajaxReturnError("moduleUrl字段非法");
        }
        if(!isString($syncFile)){
            $this->ajaxReturnError("syncFile字段非法");
        }
        if(!isDeleteInt($isDelete)){
            $this->ajaxReturnError("isDelete字段非法");
        }

        //获取模板对象
        $dao = new \Admin\Model\ModuleModel();
        //判断是否已经存在
        if(count($dao->isItemExist($moduleKey, $moduleName)) > 0){
            $this->ajaxReturnError("moduleKey或者moduleName已经存在");
        }
        //新增
        if($dao->addItem($moduleKey, $moduleName, $moduleUrl, $syncFile, $isDelete)){
            $this->ajaxReturnSuccess();
        }else{
            $this->ajaxReturnError("新增失败");
        }
    }

    public function delItem(){
        parent::delItem();
        $moduleId = intval(trim(I("moduleId", "")));
        $dao = new \Admin\Model\ModuleModel();

        if(!isPositiveInt($moduleId)){
            $this->ajaxReturnError("moduleId字段非法");
        }

        $result = $dao->delItem($moduleId);
        if($result){
            $this->ajaxReturnSuccess();
        }else{
            $this->ajaxReturnError("删除失败");
        }
    }

    public function getItemList(){
        parent::getItemList();
        $page = intval(trim(I("page", 1)));
        $moduleKey = trim(I("moduleKey", ""));
        $moduleName = trim(I("moduleName", ""));
        $isDelete = intval(trim(I("isDelete", 0)));

        if(!isPositiveInt($page)){
            $this->ajaxReturnError("page字段非法");
        }
        if(!isString($moduleKey)){
            $this->ajaxReturnError("moduleKey字段非法");
        }
        if(!isString($moduleName)){
            $this->ajaxReturnError("moduleName字段非法");
        }
        if(!isDeleteInt($isDelete)){
            $this->ajaxReturnError("isDelete字段非法");
        }

        $dao = new \Admin\Model\ModuleModel();
        $list = $dao->getItemList($moduleKey, $moduleName, $isDelete, $page);
        if($list!==false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function getItemDetail(){
        parent::getItemDetail();

        $moduleId = intval(trim(I("moduleId", "")));

        if(!isPositiveInt($moduleId)){
            $this->ajaxReturnError("moduleId字段非法");
        }
        $dao = new \Admin\Model\ModuleModel();
        $list = $dao->getItemDetail($moduleId);
        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function editItem(){
        parent::editItem();

        //获取参数
        $moduleId = intval(trim(I("moduleId", "")));
        $moduleName = trim(I("moduleName", ""));
        $moduleUrl = trim(I("moduleUrl", ""));
        $syncFile = trim(I("syncFile", ""));
        $isDelete = intval(trim(I("isDelete", 0)));

        if(!isPositiveInt($moduleId)){
            $this->ajaxReturnError("moduleId字段非法");
        }

        if(!isNoEmptyString($moduleName)){
            $this->ajaxReturnError("moduleName字段非法");
        }
        if(!isString($moduleUrl)){
            $this->ajaxReturnError("moduleUrl字段非法");
        }
        if(!isString($syncFile)){
            $this->ajaxReturnError("syncFile字段非法");
        }
        if(!isDeleteInt($isDelete)){
            $this->ajaxReturnError("isDelete字段非法");
        }

        $dao = new \Admin\Model\ModuleModel();
        $list = $dao->editItem($moduleId, $moduleName, $moduleUrl, $syncFile, $isDelete);
        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("编辑失败");
        }
    }
}