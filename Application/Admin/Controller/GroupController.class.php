<?php
namespace Admin\Controller;

use \Common\Controller\SoprController;

class GroupController extends SoprController{
    const MODULE_KEY = 'Group';
    public function index(){
        parent::index();
        $this->display();
    }
    
    public function addItem(){
        parent::addItem();
        //获取参数
        $groupName = trim(I("groupName", ""));
        $approver = trim(I('approver', ""));
        $isDelete = intval(trim(I("isDelete", 0)));
        $moduleIds = trim(I("moduleIds"), "");



        if(!isNoEmptyString($groupName)){
            $this->ajaxReturnError("groupName字段非法");
        }
        if(!isDeleteInt($isDelete)){
            $this->ajaxReturnError("isDelete字段非法");
        }

        if(is_string($moduleIds)){
            $moduleIdArr = explode(',', $moduleIds);
            //如果最后有一个逗号，则删除最后一个空元素
            if(trim($moduleIdArr[count($moduleIdArr) - 1]) == ''){
                array_pop($moduleIdArr);
            }
            //如果中间任何一个元素是非法的，则提示错误
            for($i = 0, $len = count($moduleIdArr); $i< $len; $i++){
                if (!isPositiveInt(intval(trim($moduleIdArr[$i])))){
                    $this->ajaxReturnError("moduleIds字段非法");
                    break;
                }
            }
        }else{
            $this->ajaxReturnError("moduleIds字段非法");
        }

        //获取模板对象
        $dao = new \Admin\Model\GroupModel();
        //判断是否已经存在
        if(count($dao->isItemExist($groupName)) > 0){
            $this->ajaxReturnError("用户组已经存在");
        }

        if(count($moduleIdArr) > 0){
            $dao->addItemTrans($groupName, $approver, $isDelete);
        }
        //新增
        if($dao->addItem($groupName, $approver, $isDelete)){
            $this->ajaxReturnSuccess();
        }else{
            $this->ajaxReturnError("新增失败");
        }
    }
    
    public function delItem(){
        parent::delItem();
        $groupId = intval(trim(I("groupId", "")));
        $dao = new \Admin\Model\GroupModel();

        if(!isPositiveInt($groupId)){
            $this->ajaxReturnError("groupId字段非法");
        }

        $result = $dao->delItem($groupId);
        if($result){
            $this->ajaxReturnSuccess();
        }else{
            $this->ajaxReturnError("删除失败");
        }
    }
    
    public function getItemList(){
        parent::getItemList();
        $page = intval(trim(I("page", 1)));
        $groupName = trim(I("groupName", ""));
        $isDelete = intval(trim(I("isDelete", 0)));

        if(!isPositiveInt($page)){
            $this->ajaxReturnError("page字段非法");
        }
        if(!isString($groupName)){
            $this->ajaxReturnError("groupName字段非法");
        }
        if(!isDeleteInt($isDelete)){
            $this->ajaxReturnError("isDelete字段非法");
        }

        $dao = new \Admin\Model\GroupModel();
        $list = $dao->getItemList($groupName, $isDelete, $page);
        if($list!==false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function getItemDetail(){
        parent::getItemDetail();

        $groupId = intval(trim(I("groupId", "")));

        if(!isPositiveInt($groupId)){
            $this->ajaxReturnError("groupId字段非法");
        }
        $dao = new \Admin\Model\GroupModel();
        $list = $dao->getItemDetail($groupId);
        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }
    
    public function editItem(){
        parent::editItem();

        $groupId = intval(trim(I("groupId", "")));
        $groupName = trim(I("groupName", ""));
        $approver = trim(I("approver", ""));
        $isDelete = intval(trim(I("isDelete", 0)));

        if(!isPositiveInt($groupId)){
            $this->ajaxReturnError("groupId字段非法");
        }
        if(!isNoEmptyString($groupName)){
            $this->ajaxReturnError("groupName字段非法");
        }
        if(!isDeleteInt($isDelete)){
            $this->ajaxReturnError("isDelete字段非法");
        }

        $dao = new \Admin\Model\GroupModel();
        $list = $dao->editItem($groupId, $groupName, $approver, $isDelete);
        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("编辑失败");
        }
    }
}