<?php
namespace Admin\Controller;

use \Common\Controller\SoprController;

class UserController extends SoprController{
    const MODULE_KEY = 'User';
    public function index(){
        parent::index();
        $this->display();
    }

    public function addItem(){
        parent::addItem();
        //获取参数
        $username = trim(I("username", ""));
        $groupId = intval(trim(I('groupId', "")));
        $isDelete = intval(trim(I("isDelete", 0)));

        if(!isNoEmptyString($username)){
            $this->ajaxReturnError("username字段非法");
        }
        if(!isPositiveInt($groupId)){
            $this->ajaxReturnError("groupId字段非法");
        }
        if(!isDeleteInt($isDelete)){
            $this->ajaxReturnError("isDelete字段非法");
        }

        //获取模板对象
        $dao = new \Admin\Model\UserModel();
        //判断是否已经存在
        if(count($dao->isItemExist($username)) > 0){
            $this->ajaxReturnError("用户".$username."已经存在");
        }
        //新增
        if($dao->addItem($username, $groupId, $isDelete)){
            $this->ajaxReturnSuccess();
        }else{
            $this->ajaxReturnError("新增失败");
        }
    }

    public function delItem(){
        parent::delItem();
        $username = trim(I("username", ""));
        $dao = new \Admin\Model\UserModel();

        if(!isNoEmptyString($username)){
            $this->ajaxReturnError("username字段非法");
        }

        $result = $dao->delItem($username);
        if($result){
            $this->ajaxReturnSuccess();
        }else{
            $this->ajaxReturnError("删除失败");
        }
    }

    public function getItemList(){
        parent::getItemList();
        $page = intval(trim(I("page", 1)));
        $username = trim(I("username", ""));
        $isDelete = intval(trim(I("isDelete", 0)));

        if(!isPositiveInt($page)){
            $this->ajaxReturnError("page字段非法");
        }
        if(!isString($username)){
            $this->ajaxReturnError("username字段非法");
        }
        if(!isDeleteInt($isDelete)){
            $this->ajaxReturnError("isDelete字段非法");
        }

        $dao = new \Admin\Model\UserModel();
        $list = $dao->getItemList($username, $isDelete, $page);
        if($list!==false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function getItemDetail(){
        parent::getItemDetail();

        $username = trim(I("username", ""));

        if(!isNoEmptyString($username)){
            $this->ajaxReturnError("username字段非法");
        }
        $dao = new \Admin\Model\UserModel();
        $list = $dao->getItemDetail($username);
        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function editItem(){
        parent::editItem();

        $username = trim(I("username", ""));
        $groupId = intval(trim(I("groupId", "")));
        $isDelete = intval(trim(I("isDelete", 0)));

        if(!isPositiveInt($groupId)){
            $this->ajaxReturnError("groupId字段非法");
        }
        if(!isNoEmptyString($username)){
            $this->ajaxReturnError("username字段非法");
        }
        if(!isDeleteInt($isDelete)){
            $this->ajaxReturnError("isDelete字段非法");
        }

        $dao = new \Admin\Model\UserModel();
        $list = $dao->editItem($username, $groupId, $isDelete);
        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("编辑失败");
        }
    }
}