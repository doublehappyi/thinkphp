<?php
namespace Admin\Model;

use \Common\Model\SoprModel;

class GroupModel extends SoprModel
{
    protected $trueTableName = 'sopr_group';

    /**
     *新增条目
     **/
    public function addItem($groupName, $approver, $isDelete)
    {
        $createTime = getCurrentDatetime();
        $sql = "insert into sopr_group values (null, '%s', '%s', '%s', %s)";
        $sql = sprintf($sql, $groupName, $approver, $createTime, $isDelete);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取列表
     **/
    public function getItemList($groupName, $isDelete, $page)
    {
        $sql = "select groupId, groupName, approver, createTime, isDelete
        from sopr_group
        where groupName like '%%%s%%' and isDelete=%s limit %s, %s";
        $sql = sprintf($sql, $groupName, $isDelete, ($page - 1) * $this::PAGE_SIZE, $this::PAGE_SIZE);

        $totalNumSql = "select count(groupId) as totalNum
        from sopr_group
        where groupName like '%%%s%%' and isDelete=%s";
        $totalNumSql = sprintf($totalNumSql, $groupName, $isDelete);

        try {
            $list = $this->query($sql);
            $totalNum = $this->query($totalNumSql);
            $totalNum = $totalNum[0]["totalnum"];
            $totalPage = round($totalNum/$this::PAGE_SIZE);
            return array("list"=>$list, "totalNum"=>$totalNum[0], "page"=>$page, "totalPage"=>$totalPage);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取详情
     **/
    public function getItemDetail($groupId)
    {
        $sql = "select groupId, groupName, approver, createTime, isDelete from sopr_group where groupId=%s";
        $sql = sprintf($sql, $groupId);

        try {
            $list = $this->query($sql);
            return $list[0];
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *更新条目
     **/
    public function editItem($groupId, $groupName, $approver, $isDelete)
    {
        $sql = "update sopr_group set groupName='%s', approver='%s', isDelete=%s where groupId=%s";
        $sql = sprintf($sql, $groupName, $approver, $isDelete, $groupId);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *删除条目
     **/
    public function delItem($groupId)
    {
        $sql = "update sopr_group set isDelete=1 where groupId=%s";
        $sql = sprintf($sql, $groupId);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *删判断groupName是否已经存在
     **/
    public function isItemExist($groupName)
    {
        $sql = "select 1 from sopr_group where groupName='%s'";
        $sql = sprintf($sql, $groupName);
        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }
}