<?php
namespace Admin\Model;

use \Common\Model\SoprModel;

class UserModel extends SoprModel
{
    protected $trueTableName = 'sopr_user';

    /**
     *新增条目
     **/
    public function addItem($username, $groupId, $isDelete)
    {
        $createTime = getCurrentDatetime();
        $sql = "insert into sopr_user values ('%s', %s, '%s', %s)";
        $sql = sprintf($sql, $username, $groupId, $createTime, $isDelete);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取列表
     **/
    public function getItemList($username, $isDelete, $page)
    {
        $sql = "select username, groupId, createTime, isDelete
        from sopr_user
        where username like '%%%s%%' and isDelete=%s limit %s, %s";
        $sql = sprintf($sql, $username, $isDelete, ($page - 1) * $this::PAGE_SIZE, $this::PAGE_SIZE);

        $totalNumSql = "select count(groupId) as totalNum
        from sopr_user
        where username like '%%%s%%' and isDelete=%s";
        $totalNumSql = sprintf($totalNumSql, $username, $isDelete);

        try {
            $list = $this->query($sql);
            $totalNum = $this->query($totalNumSql);
            $totalNum = $totalNum[0]["totalnum"];
            $totalPage = round($totalNum/$this::PAGE_SIZE);
            return array("list"=>$list, "totalNum"=>$totalNum[0], "page"=>$page, "totalPage"=>$totalPage);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取详情
     **/
    public function getItemDetail($username)
    {
        $sql = "select username, groupId, createTime, isDelete from sopr_user where username='%s'";
        $sql = sprintf($sql, $username);

        try {
            $list = $this->query($sql);
            return $list[0];
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *更新条目
     **/
    public function editItem($username, $groupId, $isDelete)
    {
        $sql = "update sopr_user set groupId='%s', isDelete=%s where username='%s'";
        $sql = sprintf($sql, $groupId, $isDelete, $username);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *删除条目
     **/
    public function delItem($username)
    {
        $sql = "update sopr_user set isDelete=1 where username='%s'";
        $sql = sprintf($sql, $username);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *判断username是否已经存在
     **/
    public function isItemExist($username)
    {
        $sql = "select 1 from sopr_user where username='%s'";
        $sql = sprintf($sql, $username);
        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }
}