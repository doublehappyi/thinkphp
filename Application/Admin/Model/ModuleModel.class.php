<?php
namespace Admin\Model;

use \Common\Model\SoprModel;

class ModuleModel extends SoprModel
{
    protected $trueTableName = 'sopr_module';

    /**
     *新增条目
     **/
    public function addItem($moduleKey, $moduleName, $moduleUrl, $syncFile, $isDelete)
    {
        $createTime = getCurrentDatetime();
        $sql = "insert into sopr_module values (null, '%s', '%s', '%s', '%s', '%s', %s)";
        $sql = sprintf($sql, $moduleKey, $moduleName, $moduleUrl, $syncFile, $createTime, $isDelete);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取列表
     **/
    public function getItemList($moduleKey, $moduleName, $isDelete, $page)
    {
        $sql = "select moduleId, moduleKey, moduleName, moduleUrl, syncFile, createTime, isDelete
        from sopr_module
        where moduleKey like '%%%s%%' and moduleName like '%%%s%%' and  isDelete=%s limit %s, %s";
        $sql = sprintf($sql, $moduleKey, $moduleName, $isDelete, ($page - 1) * $this::PAGE_SIZE, $this::PAGE_SIZE);

        $totalNumSql = "select count(moduleId) as totalNum
        from sopr_module
        where moduleKey like '%%%s%%' and moduleName like '%%%s%%' and  isDelete=%s";
        $totalNumSql = sprintf($totalNumSql, $moduleKey, $moduleName, $isDelete);

        try {
            $list = $this->query($sql);
            $totalNum = $this->query($totalNumSql);
            $totalNum = $totalNum[0]["totalnum"];
            $totalPage = round($totalNum/$this::PAGE_SIZE);
            return array("list"=>$list, "totalNum"=>$totalNum[0], "page"=>$page, "totalPage"=>$totalPage);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取详情
     **/
    public function getItemDetail($moduleId)
    {
        $sql = "select moduleId, moduleKey, moduleName, moduleUrl, syncFile, createTime, isDelete from sopr_module where moduleId=%s";
        $sql = sprintf($sql, $moduleId);

        try {
            $list = $this->query($sql);
            return $list[0];
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *更新条目
     **/
    public function editItem($moduleId, $moduleName, $moduleUrl, $syncFile, $isDelete)
    {
        $sql = "update sopr_module set moduleName='%s', moduleUrl='%s', syncFile='%s', isDelete=%s where moduleId=%s";
        $sql = sprintf($sql, $moduleName, $moduleUrl, $syncFile, $isDelete, $moduleId);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *删除条目
     **/
    public function delItem($moduleId)
    {
        $sql = "update sopr_module set isDelete=1 where moduleId=%s";
        $sql = sprintf($sql, $moduleId);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *删判断moduleKey, 或者moduleName是否已经存在
     **/
    public function isItemExist($moduleKey, $moduleName)
    {
        $sql = "select 1 from sopr_module where moduleKey='%s' or moduleName='%s'";
        $sql = sprintf($sql, $moduleKey, $moduleName);
        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }
}